function dy = pesticides(t,y)

dy = zeros(5,1); %initiate dy with zeros

%Set parameter value
beta = 0.30; %biting rate, given as 0.13-0.47
gamma = 1; %fraction of infectious bites, for now all bites are
           %infectious
H = 100;
rho = 0.05; %death/recovery rate for infected humans, should be
            %less than mu since humans die slower
mu = 0.15; %death/recovery rate for infected mosquitos, given as
           %0.06-0.28
pii = 0.3; %additional death/recovery rate due to pesticides
kappa = 1; %innate growth rate for mosquitoes, guessed
epsilon = 0.1; %fraction of wild offspring born mutated
P = 1000; %carying capacity for mosquitoes

%Note that M is no longer constant

%y(1) = h, fraction of infected human
%y(2) = W_i, total number of infected mosquitos (wild type)
%y(3) = M_i, total number of infected mosquitos (mutant)
%y(4) = W?
%y(5) = M?

dy(1) = beta*gamma*(y(2)+y(3))/H*(1-y(1)) - rho*y(1);
dy(2) = beta*(y(4)-y(2))*y(1) - (pii+mu)*y(2);
dy(3) = beta*(y(5)-y(3))*y(1) - mu*y(3);
dy(4) = kappa*(1-epsilon)*y(4)*(1-(y(4)+y(5))/P) - (pii+mu)*y(4);
dy(5) = kappa*y(5)*(1-(y(4)+y(5))/P)-mu*y(5) + epsilon*kappa*y(4)* ...
        (1-(y(4)+y(5))/P);

%call with
%[T,Y] = ode45(@pesticides,[0 12],[0.5 0.75*1000*0.9 0.75*1000*0.1 1000 0]);
%
%plot with
%plot(T,Y(:,1),'g-',T,Y(:,2),'r-.',T,Y(:,3),'k--',T,Y(:,4),T,Y(:,5))
