function dreide

close all

%% Parameters
H = 1000;
M = 10000;
gamma = 0.5;
beta = 0.1;
rho = 0.0035;
mu = 0.1;
mu1 = 0.1;
pi = 0.5;
%pi = 1.5;
P = 11667;
kappa1 = 0.7;
kappa1 = 0.15
kappa2 = 0.3019;
%mu2 = 0.2;
mu2 = 0.274;
epsilon = 1.36*10^(-11);

%Fungus (will vary)
alpha = 0.5;
sigma = 0.2;
kappa = kappa1;

deltaalpha = 0;
deltasigma = 0;

%% Equilibirum values
%Pesticides
Mstar = P*(1-mu2/kappa2);
R0P = beta*beta*gamma*Mstar/mu2/rho/H
hP = (R0P-1)/(R0P-beta/mu2);

i=1;
j=1;

count = 0;

alphavec = -0.1:0.01:.5;
sigmavec = -0.1:0.01:.5;

for alpha = alphavec
  j=1;
  for sigma = sigmavec
%Fungus
Wstar(i,j) = (1-(mu+alpha)*(mu+sigma)/kappa/(mu+alpha+sigma))*P*(mu+sigma)/(mu+alpha+sigma);
R0F(i,j) = beta*beta*Wstar(i,j)/rho/(mu+alpha)/H;
hF(i,j) = (R0F(i,j)-1)/(R0F(i,j)+beta/(mu+alpha));

if R0F(i,j) <= 1;
  hF(i,j) = 0;
end

C(i,j) = hF(i,j)/hP;

%if C(i,j) <= 1;
%  C(i,j) = 0;
%else
%  C(i,j) = 1;
%end

j=j+1;
  end
i = i+1;
end

C = C';

count/length(alphavec)/length(sigmavec)

[A,S] = meshgrid(alphavec,sigmavec);

c = surf(A,S,C,'EdgeColor','none');
view(45, 30);
title('Comparing the impact of pesticides and fungus')
xlabel('Fungus exposure rate \alpha')
ylabel('Fungus-induced death rate \sigma')
zlabel('h_F^\ast / h_P^\ast')
figure
[a,b] = contourf(A,S,C);
clabel(a,b);
title('Comparing the impact of pesticides and fungus (increased mutant death rate)')
xlabel('Fungus exposure rate \alpha')
ylabel('Fungus-induced death rate \sigma')

%[D h]=contour(A,S,C);
%clabel(D,h)
%hold on
%surf(A,S,D,'EdgeColor','none');
