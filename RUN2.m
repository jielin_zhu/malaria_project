% Numerical simulation of ratio of infected human and mosquitoes
% Numerical simulation of population of wild and mutant mosquitoes
% Find the time point when mutant gets the middle of their population, to
% estimate the transition time
% Calculate the cumulative integral of infected human, another measure of
% the policy

h0 = 0;
Wi0 = 1;
Mi0 = 0;
W0 = 10000;
M0 = 0;

a1 = cell(1,nargout(@parameters1));
[a1{:}] = parameters1();
beta = a1{1};
gamma = a1{2};
mu = a1{3};
rho = a1{4};
H = a1{5};

[T,Y] = ode45(@pesticides422,[0 365*2],[h0 Wi0 Mi0 W0 M0]);
n = length(T);
R_0 = beta^2*gamma*Y(n,5)/(mu*rho*H)
SM = zeros(1,n); % proportion of infected mosquitoes in total population(wild+mutant)
for i = 1:n;
    SM(i) = (Y(i,2)+Y(i,3))/(Y(i,4)+Y(i,5));
end;
Sm = SM(n) % proportion of infected mutant mosquitoes in steady state
Sh = Y(n,1) % proportion of infected human in steady state
plot(T,Y(:,1),T,SM,'-.','LineWidth',2)
xlabel('time')
ylabel('ratio of infected humans/mosquitoes')
legend('infected human','infected mosquitoes')
%plot(T,Y(:,4),'-','LineWidth',2)
plot(T,Y(:,4),T,Y(:,5),'-.','LineWidth',2) % plot the change of wild and mutant mosquitoes
xlabel('time')
ylabel('population of mosquitoes')
legend('wild type','mutant')

% Try to find the time point when mutant mosquitoes get half of their
% population
F1 = zeros(1,n);
for i=1:n;
    F1(i) = abs(Y(i,5)-Y(n,5)/2);
end;
F2 = min(F1);
i = 1;
while (F1(i)>F2 && i<n)
    i = i+1;
end;
T(i)

% calculate cumulative integration for number of infected human
Z = cumtrapz(T,Y(:,1));
N = Z(n)*H
    

% Note: Compare the result with the basic model, under the same basic value
% of parameters and be careful with the relationship of kappa, mu and pi,
% we get a similar results.
% But it takes much longer than the first one to trigger the outbreak of
% the malaria epidemic. As at the beginning, the pesticides can control the
% transmission of malaria by killing the mosquitoes.
% If we compare the whole course with the evolution of mosquitoes, we could
% see that the outbreak happened after the mutant mosquitoes got to the
% peak.