% Numerical simulation of ratio of infected human and mosquitoes
% Numerical simulation of population of fungus-free and fungus infected
% mosquitoes
% Calculate the cumulative integral of infected human, another measure of
% the policy

h0 = 0;
Wi0 = 1;
W0 = 10000;
M0 = 0;

a1 = cell(1,nargout(@parameters1));
[a1{:}] = parameters1();
beta = a1{1};
gamma = a1{2};
mu = a1{3};
rho = a1{4};
H = a1{5};

a3 = cell(1,nargout(@parameters3));
[a3{:}] = parameters2();
alpha = a3{1}; %rate at which mosquitoes are exposed to fungus, guessed

[T,Y] = ode45(@fungus,[0 365*2],[h0 Wi0 W0 M0]);
n = length(T);
R_0 = beta^2*gamma*Y(n,3)/((mu+alpha)*rho*H)
Sh = Y(n,1) % proportion of infected human in steady state
SM = zeros(1,n); % proportion of infected mosquitoes in total population(wild+mutant)
S = zeros(1,n); 
for i = 1:n;
    SM(i) = Y(i,2)/(Y(i,3)+Y(i,4));
    S(i) = Y(i,3)+Y(i,4);
end;
Sm = SM(n) % proportion of "infectious" mosquitoes in the whole population
plot(T,Y(:,1),T,SM,'-.','LineWidth',2)
xlabel('time')
ylabel('proportion of infected humans/mosquitoes')
legend('infected human','infected mosquitoes')
% plot(T,Y(:,3),T,Y(:,4),T,S) % check the change of mosquito population
% xlabel('time')
% ylabel('population of susceptible/infected fungus mosquitoes')
% legend('susceptible mosquitoes','fungus infected mosquitoes','total number of mosquitoes')

% calculate cumulative integration for number of infected human
Z = cumtrapz(T,Y(:,1));
N = Z(n)*H

% Note: use the same initial condition with the first two models and make
% the extral death rate caused by pesticides equal to the transfer rate
% from exposed to infected by fungus, we could see the epidemic can not
% start