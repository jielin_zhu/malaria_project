function dy = no_management(t,y) %dy is a vector that contains the left-hand side of the
                                 %equations. In our case, dy(1) =
                                 %infected human, dy(2) = infected mosquitos.
dy = zeros(2,1);    % we initiate the vector with zeros.

a = cell(1,nargout(@parameters1));
[a{:}] = parameters1();
beta = a{1};
gamma = a{2};
mu = a{3};
rho = a{4};
H = a{5};
M = a{6};
%Now we set the parameter values
% beta = 0.30; %biting rate, given as 0.13-0.47
% gamma = 1; %fraction of infectious bites, for now all bites are infectious
% M = 1000; %constant total mosquito population, set to 10 mosquitos/humans
% H = 100; %constant total human population, our (randomly chosen) community size
% rho = 0.05; %death/recovery rate for infected humans, should be
            %less than mu since humans die slower
% mu = 0.15; %death/recovery rate for infected mosquitos, given as 0.06-0.28

%the equations we model are
%dh/dt = beta*gamma*m*M/H * (1-h) - rho*h
%dm/dt = beta*(1-m)*h - mu*m
%
%Matlab wants us to replace h with y(1) and m with y(2). This gives
dy(1) = beta*gamma*y(2)*M/H*(1-y(1))-rho*y(1);
dy(2) = beta*(1-y(2))*y(1)-mu*y(2);

%Call this function with the command
%[T,Y] = ode45(@no_management,[0 12],[0.5 0.75]);
%Okay, this looks complicated. How does it work?
%
%LHS: -> The time steps (which are non-uniform and done by Matlab
%automatically) are saved in the vector T. The function values,
%which is what we are interested in, are saved in the vector Y. In
%fact, Y is realized as a matrix for convenience. The first column
%corresponds to the first population, i.e. the fraction of infected
%humans. The second column corresponds to the second
%population, i.e. the fraction of infected mosquitos.
%
%RHS: -> we call the famous solver 'ode45', which takes three
%arguments. The first argument is a matlab file with the name
%'no_management.m'. This file needs to contain a function also
%called 'no_management' which describes the ode we want to solve
%numerically. The second argument is the time interval we want to
%solve the ode in. Here we chose 0 \leq t \leq 12. The third
%argument is the initial condition. Here we chose h(0) = 0.5 and
%m(0) = 0.75.
%
%
%Finally, to plot the results write
%plot(T,Y(:,1),T,Y(:,2))
%or the more colorful
%plot(T,Y(:,1),'g-',T,Y(:,2),'r-.','Linewidth',2)
%where the green line is the fraction of infected humans and the
%thicker red line is the fraction on infected mosquitos.


