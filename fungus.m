function dy = fungus(t,y)

dy = zeros(4,1); %initiate dy with zeros

% call for parameters to calculate R_0
a1 = cell(1,nargout(@parameters1));
[a1{:}] = parameters1();
beta = a1{1};
gamma = a1{2};
mu = a1{3};
rho = a1{4};
H = a1{5};

a2 = cell(1,nargout(@parameters2));
[a2{:}] = parameters2();
kappa = a2{1}; %innate growth rate for mosquitoes, guessed
P = a2{4}; %carying capacity for mosquitoes

a3 = cell(1,nargout(@parameters3));
[a3{:}] = parameters3();
alpha = a3{1}; %rate at which mosquitoes are exposed to fungus, guessed
pi = a3{2};

%Note that M is no longer constant

%y(1) = h, fraction of infected human
%y(2) = W_i, total number of infected mosquitos (wild type)
%y(3) = W?
%y(4) = M, total population infected with fungus

dy(1) = beta*gamma*y(2)/H*(1-y(1)) - rho*y(1);
dy(2) = beta*(y(3)-y(2))*y(1) - mu*y(2)-alpha*y(2);
dy(3) = kappa*y(3)*(1-(y(3)+y(4))/P) - mu*y(3)-alpha*y(3);
dy(4) = alpha*y(3) - mu*y(4) - pi*y(4);

%call with
%[T,Y] = ode45(@fungus,[0 12],[0.5 0.75*1000 1000 0]);
%
%plot with
%plot(T,Y(:,1),'g-',T,Y(:,2),'r-.',T,Y(:,3),'k--')
