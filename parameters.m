function [beta,gamma,mu,v1,v2,rho,H,M,P,pi,kappa,epsilon,W0,M0,h0,m0,Wi0,Mi0,alpha] = parameters

% For the basic model
beta = 0.4; %1 beta is the number of times one mosquito would bite humans per unit time, range (0.1,1)
gamma = 0.022; %2 gamma is the probability of transmission of infection from an infectious mosquito to a susceptible human, range (0.01,0.27)
mu = 0.033 %3 mu is the naturak death rate of mosquitoes, range (0.001,0.1)
v1 = 0.1; %4 1/v1 is the average period from exposed state to the infectious state
v2 = 0.0035; %5 1/v2 is the average period from infectious state to the recovered state
rho = 0.05;%6 0.0034=1/(1/v1+1/v2) % 1/rho is the average period for infection; 
H = 1000; %7 H is the total number of human (constant)
M = 10000; %8 M is the total number of mosquitoes (constant)

%Extra for pesticides
P = M; %9 carrying capacity of mosquito
pi = 0.1; %10 extra death rate caused by pesticides;
kappa = 0.15; %11 growth rate for mosquitoes, range (0.02,0.27);
epsilon = 10^(-4); %12 mutation rante for mosquito caused by pesticides;
W0 = 10000; %13 W is the total number of wild mosquitoes (function of t)
M0 = 0; %14 M is the total number of mutant mosquitoes (function of t)
h0 = 0.6079; %15 change the initial condition as the equilibrium of basic model
m0 = 10^(-4); %16 m is the proportion of infected mosquitoes in the total population

%equilibirium values from basic model
Wi0 = 0.8799*W0; %17 change the initial condition as the equilibrium of basic model
Mi0 = 0; %18 Mi is the number of infected mutant mosquitoes in the total population at the beginning

%Extra for fungus
alpha = 0.1; %19 rate at which mosquitoes are exposed to fungus;