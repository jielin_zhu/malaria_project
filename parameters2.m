% define extra value of parameters for model with pesticides
function [kappa1,kappa2,epsilon,P,pi,mu1,mu2] = parameters2

a = cell(1,nargout(@parameters1));
[a{:}] = parameters1();

kappa1 = 0.15;
kappa2 = 0.13;
epsilon = 10^(-4);
P = a{6};
pi = 0.1; % extral death rate caused by pesticides
mu1 = 0.033;
mu2 = 0.05;

%(pi+mu1)/mu2 - kappa1/kappa2