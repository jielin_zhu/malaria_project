% define extra value of parameters for model with fungus
function [alpha, pi] = parameters3

alpha = 0.08; % transmission rate for mosquito to get fungus
pi = 0.05; % extral death rate caused by mosquitoes