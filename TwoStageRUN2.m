% Combine two ode45 to simulate pesticide switch on-off policy to deal with malaria and mosquito evolution
% call funtion pesticides422 and no_pesticides422

% Initial conditions for first stage
W0 = 10000; % W is the total number of wild mosquitoes (function of t)
M0 = 0; % M is the total number of mutant mosquitoes (function of t)
H = 1000; % H is the total number of human (constant)
% h0 = 0; % h is the proportion of infected human in the total population
% h0 = 0.6079; % change the initial condition as the equilibrium of basic model
% Wi0 = 10^(-4)*W0; % Wi is the number of infected wild mosquitoes in the total population at the beginning
% Wi0 = 0.8799*W0; % change the initial condition as the equilibrium of basic model
Mi0 = 0; % Mi is the number of infected mutant mosquitoes in the total population at the beginning

% change the initial condition as the equilibrium of basic model
h0 = 0;
m0 = 10^(-4);
[T4,Y4] = ode45(@no_management,[0 200],[h0 m0]);
m = length(T4);
h0 = Y4(m,1);
Wi0 = Y4(m,2)*W0; % change the initial condition as the equilibrium of basic model

t1 = 150; % time length for pesticide switch-on period 
[T1,Y1] = ode45(@pesticides422,[0 t1],[h0 Wi0 Mi0 W0 M0]); % simulate time series with pesticides on
n1 = length(T1);
t2 = 365*2-t1; % time length for pesticide switch-off period 
[T2,Y2] = ode45(@no_pesticides422,[0 t2],[Y1(n1,1) Y1(n1,2) Y1(n1,3) Y1(n1,4) Y1(n1,5)]); 
n2 = length(T2);

[T3,Y3] = ode45(@pesticides422,[0 t1+t2],[h0 Wi0 Mi0 W0 M0]); % control group with continuing spray pesticides


% plot(T1,Y1(:,4))

n = n1+n2; % Combine the two stages
FT = zeros(1,n); % final time series
Fh = zeros(1,n);
FW = zeros(1,n); % final time series for total population of wild mosquitoes
FM = zeros(1,n);
Fm = zeros(1,n);
for i=1:n;
    if (i<n1 || i==n1)
        FT(i) = T1(i);
        Fh(i) = Y1(i,1);
        FW(i) = Y1(i,4);
        FM(i) = Y1(i,5);
        Fm(i) = (Y1(i,2)+Y1(i,3))/(Y1(i,4)+Y1(i,5));
    else
        FT(i) = T1(n1)+T2(i-n1);
        Fh(i) = Y2(i-n1,1);
        FW(i) = Y2(i-n1,4);
        FM(i) = Y2(i-n1,5);
        Fm(i) = (Y2(i-n1,2)+Y2(i-n1,3))/(Y2(i-n1,4)+Y2(i-n1,5));
    end;
end;

plot(FT,Fh,FT,Fm,'.',T3,Y3(:,1),'-.')
xlabel('time')
ylabel('ratio of infected humans/mosquitoes')
legend('infected human','infected mosquitoes','comparable group of infected human')
% plot(FT,FW,FT,FM)
% plot(T3,Y3(:,4),T3,Y3(:,5))
% plot(T3,Y3(:,1))

% calculate cumulative integration for number of infected human
Z = cumtrapz(FT,Fh);
N = Z(n)*H