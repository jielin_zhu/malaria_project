% Test basic model with estimates of parameters

%Set initial conditions for the system
h0 = 0;
m0 = 10^(-4);

% call for parameters to calculate R_0
a = cell(1,nargout(@parameters1));
[a{:}] = parameters1();
beta = a{1};
gamma = a{2};
mu = a{3};
rho = a{4};
H = a{5};
M = a{6};

[T,Y] = ode45(@no_management,[0 200],[h0 m0]);
R_0 = beta^2*gamma*M/(mu*rho*H)

figure;
plot(T,Y(:,1),T,Y(:,2),'-.','Linewidth',2)
xlabel('time')
ylabel('proportion of infected humans/mosquitoes')
legend('proportion of infected human','proportion of infected mosquitoes')

% calculate cumulative integration for number of infected human
Z = cumtrapz(T,Y(:,1));
n = length(T);
N = Z(n)*H