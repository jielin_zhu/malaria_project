function dy = PTest(t,y,flag,kappa1)

dy = zeros(2,1);

a2 = cell(1,nargout(@parameters2));
[a2{:}] = parameters2();

% kappa1 = a2{1}; %innate growth rate for wild mosquitoes
kappa2 = a2{2}; %innate growth rate for mutant mosquitoes, which should be smaller than kappa1 because of evolution advantage
% Need to make sure kappa1/2 > mu+pi to get positive population for
% mosquitoes
epsilon = a2{3}; %fraction of wild offspring born mutated
P = a2{4}; %carying capacity for mosquitoes
pii = a2{5}; %additional death/recovery rate due to pesticides
mu1 = a2{6}; %death/recovery rate for infected mosquitos, given as
           %0.06-0.28
mu2 = a2{7}; % death rate for mutant mosquitoes

dy(1) = kappa1*(1-epsilon)*y(1)*(1-(y(1)+y(5))/P) - (pii+mu1)*y(1);
dy(2) = kappa2*y(2)*(1-(y(1)+y(2))/P)-mu2*y(2) + epsilon*kappa1*y(1)*(1-(y(1)+y(2))/P);