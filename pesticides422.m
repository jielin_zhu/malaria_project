% ode system for malaria epidemic with effect of pesticide and mosquito
% evoluation

function dy = pesticides422(t,y)

dy = zeros(5,1); %initiate dy with zeros

a1 = cell(1,nargout(@parameters1));
[a1{:}] = parameters1();
a2 = cell(1,nargout(@parameters2));
[a2{:}] = parameters2();

%Set parameter value
beta = a1{1};
gamma = a1{2};
rho = a1{4};
H = a1{5};

kappa1 = a2{1}; %innate growth rate for wild mosquitoes
kappa2 = a2{2}; %innate growth rate for mutant mosquitoes, which should be smaller than kappa1 because of evolution advantage
% Need to make sure kappa1/2 > mu+pi to get positive population for
% mosquitoes
epsilon = a2{3}; %fraction of wild offspring born mutated
P = a2{4}; %carying capacity for mosquitoes
pii = a2{5}; %additional death/recovery rate due to pesticides
mu1 = a2{6}; %death/recovery rate for infected mosquitos, given as
           %0.06-0.28
mu2 = a2{7}; % death rate for mutant mosquitoes

%Note that M is no longer constant

%y(1) = h, fraction of infected human
%y(2) = W_i, total number of infected mosquitos (wild type)
%y(3) = M_i, total number of infected mosquitos (mutant)
%y(4) = W, total number of wild mosquitoes
%y(5) = M, total number of mutant mosquitoes

dy(1) = beta*gamma*(y(2)+y(3))/H*(1-y(1)) - rho*y(1);
dy(2) = beta*(y(4)-y(2))*y(1) - (pii+mu1)*y(2);
dy(3) = beta*(y(5)-y(3))*y(1) - mu2*y(3);
dy(4) = kappa1*(1-epsilon)*y(4)*(1-(y(4)+y(5))/P) - (pii+mu1)*y(4);
% dy(4) = kappa1*abs(sin(2*pi*t/360))*(1-epsilon)*y(4)*(1-(y(4)+y(5))/P) - (pii+mu)*y(4); % with time dependent birth rate
dy(5) = kappa2*y(5)*(1-(y(4)+y(5))/P)-mu2*y(5) + epsilon*kappa1*y(4)*(1-(y(4)+y(5))/P);
% dy(5) = kappa2*abs(sin(2*pi*t/360))*y(5)*(1-(y(4)+y(5))/P)-mu*y(5) + epsilon*kappa1*y(4)*(1-(y(4)+y(5))/P);